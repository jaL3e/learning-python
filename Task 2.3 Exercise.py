#Type the following statements in the Python interpreter to see what they do
print('>>> Exercise 1')
5
x = 5
x+1




#Write a program that uses input to prompt a user for their name and then welcomes them.
print ('-----------------------------------')
print('proceed to >>> Exercise 2')
nam= input('Enter your name')
print('Hello', nam)




#Write a program to prompt the user for hours and rate per hour to compute gross pay
print ('-----------------------------------')
print('proceed to >>> Exercise 3')
hr = float(input ('Enter hours:'))
rt = float(input ('Enter rate:'))
print('Pay:', (hr * rt))



#Assume that we execute the following assignment statements
print ('-----------------------------------')
print('proceed to >>> Exercise 4')

width = 17
height = 12.0

ans = width//2
print ('answer ', ans)
print ('type of answer', type(ans))

ans2 = width/2.0
print ('answer ', ans2)
print ('type of answer2', type(ans2))

ans3 = height/3
print ('answer ', ans3)
print ('type of answer', type(ans3))

ans4 = 1 + 2 * 5
print ('answer ', ans4)
print ('type of answer', type(ans4))



#Write a program which prompts the user for a Celsius temperature, convert the temperature to Fahrenheit, and print out the converted temperature.
print ('-----------------------------------')
print('proceed to >>> Exercise 5')

celtemp = float(input('Enter Celsius temperature:'))


far = (celtemp * 9/5)+32
print('Fahrenheit:', far)


