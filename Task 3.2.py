#Exercise 1: Rewrite your pay computation to give the employee 1.5 times the hourly rate for hours worked above 40 hours.

'''hr = float(input ('Enter hours:'))
rt = float(input ('Enter rate:'))

if (hr <= 40):

    print('Pay:', (hr * rt))
else:
    pay = (40 * rt) + ((hr - 40 ) * (rt * 1.5))
    print ('Pay:', pay)'''

#Rewrite your pay program using try and except so that your program handles non-numeric input gracefully by printing a message and exiting the program. The following shows two executions of the program:

import sys
try:
    hr = float(input ('Enter hours:'))
    rt = float(input ('Enter rate:'))
except:
    print('Invalid input!')
    sys.exit()

if (hr <= 40):

    print('Pay:', (hr * rt))
else:
    pay = (40 * rt) + ((hr - 40 ) * (rt * 1.5))
    print ('Pay:', pay)


#Exercise 3: Write a program to prompt for a score between 0.0 and 1.0. If the score is out of range, print an error message. If the score is between 0.0 and 1.0, print a grade using the following table:

'''score = float(input('Enter a score:'))
y = 0.0
x = 1.0

if (score > x):
    print('Bad score')
else:

    if (score >= 0.9):
        print('A')
    elif (score >= 0.8):
        print('B')
    elif (score >= 0.7 ):
        print('C')
    elif (score >= 0.6 ):
        print('D')
    elif (score < 0.6):
        print('F')'''

